<?php

namespace Drupal\highcharts_intg\Plugin\Block;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @Block(
 *      id = "highcharts_plugin",
 *      admin_label = "Highcharts Plugin",
 *      category = "Custom"
 * )
 */
class HighchartsPluginBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        return ['fid' => NULL];
    }

    /**
     * {@inheritdoc}
     */
    public function build() {
        $config = $this->getConfiguration();
        $fid = $config['fid'];
        $highchart_libraries[] = 'highcharts_intg/global';
        foreach ($config['libraries'] as $library) {
            switch ($library) {
                case 'export':
                    $highchart_libraries[] = 'highcharts_intg/highcharts_lib.export';
                    break;
                case 'data_export':
                    $highchart_libraries[] = 'highcharts_intg/highcharts_lib.export_data';
                    break;
                case 'series_label':
                    $highchart_libraries[] = 'highcharts_intg/highcharts_lib.series_label';
                    break;
                case 'accessibility':
                    $highchart_libraries[] = 'highcharts_intg/highcharts_lib.accessibility';
                    break;
            }
        }
        return [
            '#theme' => 'highcharts_block',
            '#fid' => $fid,
            '#attached' => [
                'library' => $highchart_libraries
            ]
        ];
    }

    public function blockForm($form, FormStateInterface $form_state): array {

        $form['file_upload'] = [
            '#type' => 'file',
            '#title' => 'Data upload',
            '#size' => 40,
            '#description' => t('Upload only csv file')
        ];
        $form['fid'] = ['#type' => 'hidden'];
        $form['uploaded_filename'] = ['#type' => 'hidden'];
        $form['libraries'] = [
            '#type' => 'select',
            '#title' => 'Library',
            '#options' => ['export' => 'Export', 'serial_label' => 'Serial Label', 'accessibility' => 'Accessibility', 'data_export' => 'Data Export'],
            '#multiple' => TRUE,
            '#default_value' => $this->configuration['libraries']
        ];
        return $form;
    }

    public function blockValidate($form, FormStateInterface $form_state) {
        $file_name = $form_state->getValue('file_upload');

        $file = file_save_upload('settings', [], 'public://upload');

        if (!$file_name) {
            $form_state->setErrorByName('file_upload', t('Please upload the CSV file'));
        } else if ($file[0]->getMimeType() != 'text/csv') {
            $form_state->setErrorByName('file_upload', t('Please upload CSV format file'));
        } else {
            $form_state->setValue('uploaded_filename', $file[0]->getFileUri());
            $form_state->setValue('fid', $file[0]->id());
        }       
    }

    public function blockSubmit($form, FormStateInterface $form_state) {        
        $libraries = $form_state->getValue('libraries');
        $fid = $form_state->getValue('fid');        
        if (!is_null($fid)) {
            $uploaded_filename = $form_state->getValue('uploaded_filename');
            $this->configuration['fid'] = $fid;
            $config = \Drupal::service('config.factory')->getEditable('highcharts.config');
            $config->set("fileuri$fid", $uploaded_filename)->save();
        }
        $this->configuration['libraries'] = $libraries;
    }
}
